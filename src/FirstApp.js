const FirstApp = ({d = 'value for default'}) => {
  const objectX = {
    name: 'Jarvis',
    old: 100
  };

  return (
    <>
      <pre> {JSON.stringify(objectX, null, 1)}</pre>
      <h1>{d}</h1>
    </>
  );
};

export { FirstApp };
