import { useState } from 'react';
import PropTypes from 'prop-types';

const ConuterApp = ({ d = 10 }) => {
  const [valueState, setValue] = useState(d);
  const add = () => {
    setValue(valueState + 1);
  };

  const reset = () => {
    setValue(d);
  };

  const subtract = () => {
    setValue(valueState - 1)
  };

  return (
    <>
      <h1>Counter App </h1>
      <h2>{d}</h2>
      <button onClick={add}>Add</button>
      <button onClick={reset}>Reset</button>
      <button onClick={subtract}>menos</button>
      <h3>{valueState}</h3>
    </>
  );
};

ConuterApp.propTypes = {
  d: PropTypes.number
};

export default ConuterApp;
